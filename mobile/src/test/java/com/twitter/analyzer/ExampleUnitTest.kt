package com.twitter.analyzer

import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.twitter.analyzer.ui.analyzerTheme
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 * TODO: Apparently compose can not be used within non-android tests
 */
class ExampleUnitTest {

//    @get:Rule
//    val activityScenarioRule = ActivityScenarioRule(MainActivity::class.java)

//    @get:Rule
//    val composeTestRule = createAndroidComposeRule<MainActivity>()
    // createComposeRule() if you don't need access to the activityTestRule

//    @Test
//    fun greeting_showsTextValue() {
//        // Start the app
//        composeTestRule.setContent {
//            analyzerTheme {
//                // A surface container using the 'background' color from the theme
//                Surface(color = MaterialTheme.colors.background) {
//                    Greeting("Android")
//                }
//            }
//        }
//        composeTestRule.onNodeWithText("Hello Android!").assertIsDisplayed()
//    }

    @Test
    fun test_simpleSumGivesCorrectResults() {
        assertEquals(2+2, 4)
    }
}