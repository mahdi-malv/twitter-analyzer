package com.twitter.analyzer

import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import com.twitter.analyzer.ui.analyzerTheme
import org.junit.Rule
import org.junit.Test

class FirstComposeTest {

    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()
    // createComposeRule() if you don't need access to the activityTestRule

    @Test
    fun greeting_showsTextValue() {
        // Start the app
        composeTestRule.setContent {
            analyzerTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    Greeting("Android")
                }
            }
        }
        composeTestRule.onNodeWithText("Hello Android!").assertIsDisplayed()
    }
}