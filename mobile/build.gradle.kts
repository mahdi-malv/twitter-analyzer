import org.jetbrains.kotlin.backend.wasm.lower.excludeDeclarationsFromCodegen

plugins {
    id("com.android.application")
    kotlin("android")
}

android {
    compileSdkVersion(Versions.Build.compileSdk)

    defaultConfig {
        applicationId = "com.twitter.analyzer"
        minSdkVersion(Versions.Build.minSdk)
        targetSdkVersion(Versions.Build.targetSdk)
        versionCode = Versions.versionCode
        versionName = Versions.versionName

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
        useIR = true
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = Libs.AndroidX.Compose.version
        kotlinCompilerVersion = Libs.Kotlin.version
    }
    testOptions.unitTests.isIncludeAndroidResources = true

    packagingOptions.exclude("META-INF/AL2.0")
    packagingOptions.exclude("META-INF/LGPL2.1")
}

dependencies {
    implementation(kotlin("stdlib", version = Libs.Kotlin.version))
    implementation(Libs.AndroidX.core)
    implementation(Libs.AndroidX.appCompat)
    implementation(Libs.AndroidX.material)
    implementation(Libs.AndroidX.lifecycle)
    implementation(Libs.AndroidX.Compose.ui)
    implementation(Libs.AndroidX.Compose.material)
    implementation(Libs.AndroidX.Compose.tooling)
    testImplementation(Libs.Test.junit)

    androidTestImplementation(Libs.Test.ext)
    androidTestImplementation(Libs.Test.espresso)
    androidTestImplementation(Libs.AndroidX.Compose.testing)

    testImplementation(Libs.AndroidX.Compose.testing)
}